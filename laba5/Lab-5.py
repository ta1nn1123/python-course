import random

def sortBYLength(inputStr):
    return len(inputStr)

flag = 0
fhand = open('file1.txt', 'w')
text = 'From stephen.marquard@uct.ac.za Sat Jan 5 14:16:28 2018'

fhand.write(text + '\n')

fhand.close()

lst = list(text.split(" "))

lst.sort(key=None, reverse=False)
fhand = open('file2.txt', "w")
for str in lst:
    fhand.write(str + '\n')

fhand.write("\n")

lst.sort(key=sortBYLength,reverse=False)

for str in lst:
    fhand.write(str + '\n')

fhand.close()

#
#  Кортежи
#
tuple = range(1,10)
cols = range(1,3)
for row in tuple:
    for col in cols:
        if flag == 0:
            col = random.randrange(10,30,4)
            flag = 1
        else:
            col = random.randint(0,3)
            flag = 0
        print(row, col)
