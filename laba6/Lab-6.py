text = 'To explore different supervised learning algorithms, \
we\'re going to use aп algorithms of small synthetic or artificial \
datasets as examples, together with some larger \
real world datasets'

# Функция получения последней позиции слова в тексте
def getPosWord(word):
    return text.rindex(word,0)

# Удаление запятых из текста
text = text.replace(',','')

# Создание списка из текста, разделитель пробел
lst = list(text.split(" "))

# Удаление из списка дубликатов
li = []
[li.append(x) for x in lst if x not in li]

# Создание словаря
d1 = {x:getPosWord(x) for x in li}

# Создание списка из значений словаря
li = []
[li.append(y) for x,y in d1.items()]

# Сортировка списка значений в обратном порядке
li.sort(key=None,reverse=True)

print(li)
print(d1.values())

# Создание списка используя List Comprehensions из чисел от 4 до 20 которые делятся на 3
lst1 = [x for x in [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20] if (x % 3) == 0]

print(lst1)
# Создание списка из пяти произвольных имен
lst2 = ['Вера', 'Андрей', 'Маления', 'Элемия', 'Хельга']
# Создание словаря из двух предыдущих списков
lst12 = dict(zip(lst1, lst2))

print(lst12)
